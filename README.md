# Introdução à Programação de Computadores

Curso de introdução à programação voltado a jovens das periferias recifenses.

Copie, reproduza, melhore!

Este repositório servirá para manter a organização, o planejamento e os materiais das aulas. 

Inícios: 21/08/2019
Encerramento: 20/12/2019

## Cronograma


|	Módulo 1 		          |	Módulo 2	     | Módulo 3	   | Módulo 4 	 |
|-----------------------|----------------|-------------|-------------|
| Lógica p/ Programação | Adm. Sistemas  | Programação | Programação |
| Organização de Comp.  | Programação    | Programação | Programação |
| Inglês              	| Inglês         | Inglês      | Inglês      |


Aulas: Quartas e Sextas (Informática) - 3h por dia divididas em duas disciplinas de 1:30h.
Sábados: Inglês.

Projeto final: Programar um jogo simples em Python.

Planos de aula:

  * [Lógica para Programação](https://gitlab.com/kaozhaklab/intro-2-programming/blob/master/planejamento_modulos/logica_para_programacao.md)
  * [Organização de Computadores](https://gitlab.com/kaozhaklab/intro-2-programming/blob/master/planejamento_modulos/organizacao_de_computadores.md)
  * [Administração de Sistemas]()
  * [Programação]()
  * [Inglês]()


## Referências:

[Aprendendo computação com Python](https://aprendendo-computacao-com-python.readthedocs.io/en/latest/)
