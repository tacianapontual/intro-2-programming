HakLab :: Proposta pedagogica para logica de programação

Lógica de Programção:

  * 15h = 10 aulas

Objetivo:

  * Basear o entendimento da programação como um ramo da matemática e dependente do modelo de computação em questão;
  * Introduzir conceitos de lógica proposicional e álgebra booleana;
  * Introduzir às estruturas de programação e ao conceito de algoritmo.

Planejamento de aulas:        

  * Aula 1
    * Introdução a programação de computadores; 
    * História da computação
    * Grandes 
  * Aula 2
    * Computação e Matemática
    * Número e contagem
  * Aula 3
    * Lógica proposicional
  * Aula 4
    * Exercicios
  * Aula 5
    * Álgebra Booleana
  * Aula 6
    * Exercícios
  * Aula 7
    * Modelo computacional;
  * Aula 8
    * Algoritmos;
  * Aula 9
    * Estruturas de programação (variaveis, decisão, repetição, vetores, matrizes, modulos);
  * Aula 10
    * Exercícios
